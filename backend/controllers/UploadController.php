<?php

namespace backend\controllers;

use yii\filters\AccessControl;
use yii\web\Controller;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;

/**
 * Class UploadController
 * @package backend\controllers
 */
class UploadController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['fileapi-upload',],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'fileapi-upload' => [
                'class' => FileAPIUpload::class,
                'path' => '@frontend/web//upload/temp',
                'uploadOnlyImage' => false,
            ],
        ];
    }
}

<?php

namespace backend\controllers;

use backend\forms\users\RoleForm;
use backend\forms\users\UserForm;
use backend\services\users\UsersService;
use Yii;
use common\models\users\User;
use common\models\users\UserSearch;
use yii\base\Response;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsersController implements the CRUD actions for User model.
 */
class UsersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'role-save'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserForm();

        $usersService = new UsersService(new User());
        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            try {
                $user = $usersService->create($model);
                Yii::$app->session->setFlash('success', 'Пользователь успешно создан');
                return $this->redirect(['view', 'id' => $user->id]);
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $user = $this->findModel($id);
        $model = new UserForm();
        $model->prepareCreate($user);

        $usersService = new UsersService($user);
        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            try {
                $user = $usersService->create($model);
                Yii::$app->session->setFlash('success', 'Пользователь успешно обновлен');
                return $this->redirect(['view', 'id' => $user->id]);
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        //заполняем форму Ролей
        /** @var $roleForm RoleForm */
        $roleForm = new RoleForm();
        $roleForm->prepareUpdate($user);

        return $this->render('update', [
            'model' => $model,
            'roleForm' => $roleForm,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $id
     * @return Response
     * @throws \yii\web\HttpException
     */
    public function actionRoleSave($id)
    {
        $user = $this->findModel($id);

        /**
         * @var RoleForm $roleForm
         */
        $roleForm = new RoleForm();

        if ($roleForm->load(Yii::$app->request->post())) {

            /**
             * @var $userService UsersService
             */
            $userService = new UsersService($user);

            try {
                if ($userService->setRolesData($roleForm->attributes)) {
                    Yii::$app->session->setFlash('success', 'Успешно сохранено');
                }

            } catch (\Exception $exception) {
               Yii::$app->session->setFlash('error', $exception->getMessage());
            }

            return $this->redirect(['update', 'id' => $user->id]);
        }
    }
}

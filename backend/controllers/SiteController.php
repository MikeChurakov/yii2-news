<?php

namespace backend\controllers;

use backend\services\AuthService;
use backend\forms\users\PasswordResetRequestForm;
use backend\forms\users\ResetPasswordForm;
use backend\services\users\UsersService;
use common\models\users\User;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\users\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'request-password-reset', 'reset-password'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'test'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = '@backend/views/layouts/auth';
        $model = new LoginForm();
        $authService = new AuthService($model);
        if (Yii::$app->request->isPost) {
            try {
                $result = $authService->login(Yii::$app->request->post());
                return $this->goHome();
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionTest()
    {
        return $this->render('index');
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Форма восстановления пароля
     * @return string|\yii\web\Response
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = '@backend/views/layouts/auth';
        $passwordResetForm = new PasswordResetRequestForm();
        //приняли данные пишем в модель валидируем
        if ($passwordResetForm->load(Yii::$app->request->post()) && $passwordResetForm->validate()) {
            $authService = new UsersService(new User());

            try {
                $authService->requestPasswordReset($passwordResetForm->attributes);
                Yii::$app->session->setFlash('success', 'Ссылка для восстановления пароля отправлена на ваш email.');
                return $this->redirect(['login']);

            } catch (\Exception $exception) {
                Yii::$app->session->setFlash('error', $exception->getMessage());
            }
        }

        return $this->render('request-password-reset', [
            'model' => $passwordResetForm,
        ]);
    }

    /**
     * @param string $token токен восстановления пароля
     * @return string|\yii\web\Response
     */
    public function actionResetPassword($token)
    {
        $this->layout = '@backend/views/layouts/auth';

        $model = new ResetPasswordForm();
        $model->token = $token;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $authService = new UsersService(new User());

            try {
                $authService->resetPasswordByToken($model->token, $model->attributes);
                Yii::$app->session->setFlash('success', 'пароль успешно обновлен');
                return $this->redirect('login');

            } catch (\Exception $exception) {
                Yii::$app->session->setFlash('error', $exception->getMessage());
            }
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}

<?php

namespace backend\services\news;

use common\models\news\News;
use yii\base\BaseObject;

/**
 * Class NewsService
 * @package backend\services\news
 *
 * @property News $model
 */
class NewsService extends BaseObject
{
    /**
     * @var News
     */
    public $model;

    /**
     * NewsService constructor.
     * @param News $news
     */
    public function __construct(News $news)
    {
        parent::__construct();
        $this->model = $news;
    }

    /**
     * @param array $data
     * @return News
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data)
    {
        $model = $this->model;
        $model->load($data);

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if (!$model->save()) {
                throw new \Exception(implode(',', $model->firstErrors));
            }

            $transaction->commit();
        } catch (\Throwable $exception) {
            $transaction->rollBack();
            throw $exception;
        }

        return $model;
    }

    /**
     * @param array $data
     * @return News
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function update(array $data)
    {
        $model = $this->model;
        $model->load($data);

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if (!$model->save()) {
                throw new \Exception(implode(',', $model->firstErrors));
            }

            $transaction->commit();
        } catch (\Throwable $exception) {
            $transaction->rollBack();
            throw $exception;
        }

        return $model;
    }
}

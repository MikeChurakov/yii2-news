<?php

namespace backend\services;

use yii\base\BaseObject;
use yii\base\Exception;

/**
 * Class AuthService
 * @package backend\services
 */
class AuthService extends BaseObject
{
    public $model;

    /**
     * AuthService constructor.
     * @param $model
     * @param array $config
     */
    public function __construct($model, array $config = [])
    {
        parent::__construct($config);
        $this->model = $model;
    }

    /**
     * @param array $data
     * @return bool
     * @throws Exception
     */
    public function login(array $data)
    {
        $this->model->load($data);

        if(!$this->model->validate()) {
            throw new Exception(implode(',', $this->model->firstErrors));
        }

        \Yii::$app->user->login($this->model->getUser(), $this->model->rememberMe ? 3600 * 24 * 30 : 0);

        return true;
    }
}
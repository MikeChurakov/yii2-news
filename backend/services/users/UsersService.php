<?php

namespace backend\services\users;

use backend\forms\users\PasswordResetRequestForm;
use backend\forms\users\RoleForm;
use backend\forms\users\UserForm;
use common\models\users\User;
use backend\forms\users\ResetPasswordForm;
use yii\base\BaseObject;
use yii\base\Exception;
use Yii;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

/**
 * Class UsersService
 * @package backend\services\users
 */
class UsersService extends BaseObject
{
    /**
     * @var User
     */
    public $user;

    /**
     * UsersService constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    /**
     * @param UserForm $userForm
     * @return User
     * @throws \Exception
     */
    public function create(UserForm $userForm) : User
    {
        if(!$userForm->validate()) {
            throw new \Exception(implode(',', $userForm->firstErrors));
        }

        $this->user->email = $userForm->email;
        $this->user->username = $userForm->username;
        $this->user->status = $userForm->status;

        if($this->user->isNewRecord) {
            $this->user->generateAuthKey();
        }

        if($userForm->password) {
            $this->user->password = \Yii::$app->security->generatePasswordHash($userForm->password);
        }

        if(!$this->user->save()) {
            throw new \Exception(implode(',', $this->user->firstErrors));
        }

        return $this->user;
    }

    /**
     * @param array $rolesData
     * @return bool
     * @throws Exception
     */
    public function setRolesData(array $rolesData = [])
    {
        if (empty($rolesData)) {
            return false;
        }

        /**
         * @var $roleForm RoleForm
         */
        $roleForm = new RoleForm();

        $roleForm->prepareUpdate($this->user);
        $roleForm->load($rolesData, '');
        $roleForm->id_user = $this->user->id;

        if (!$roleForm->validate()) {
            throw new Exception(implode(',', $roleForm->firstErrors));
        }

        $this->setRolesAndPermissions((array)$roleForm->roles, (array)$roleForm->permissions);

        return true;
    }

    /**
     * Устанавливаем введенные роли/права юзеру
     * @param array $roles ['user', 'admin'] Имена ролей
     * @param array $permissions ['r_site', 'w_site'] Имена прав
     * @throws \Exception
     */
    public function setRolesAndPermissions(array $roles = [], array $permissions = [])
    {
        Yii::$app->authManager->revokeAll($this->user->id);

        foreach ($roles as $role) {
            $newRole = Yii::$app->authManager->getRole($role); //создаем объект роли по имени
            if (!$newRole) {
                continue;
            }
            Yii::$app->authManager->assign($newRole, $this->user->id);
        }


        foreach ($permissions as $permission) {
            $newPermission = Yii::$app->authManager->getPermission($permission); //создаем объект роли по имени
            if (!$newPermission) {
                continue;
            }
            Yii::$app->authManager->assign($newPermission, $this->user->id);
        }
    }

    /**
     * Запрос на восстановление пароля
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function requestPasswordReset(array $data)
    {
        $modelForm = new PasswordResetRequestForm();
        $modelForm->load($data, '');
        if (!$modelForm->validate()) {
            throw new Exception(implode(',', $modelForm->firstErrors));
        }

        $user = User::find()->where(['email' => $modelForm->email])->one();
        if (!$user) {
            return true;
        }

        $user->generatePasswordResetToken();
        if(!$user->save()) {
            throw new \Exception(implode(',', $this->user->firstErrors));
        }

        $this->sendEmailResetPassword($user);

        return true;
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @param User $user
     * @return bool whether the email was send
     */
    public function sendEmailResetPassword(User $user)
    {
        $url = Yii::$app->params['domain'] . '/admin/site/reset-password?token=' . $user->password_reset_token;

        return Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['email.from'])
            ->setTo($user->email)
            ->setSubject('Восстановление пароля')
            ->setTextBody('Ваша ссылка для восстановления пароля: ' . Html::a($url, $url))
            ->setHtmlBody('Ваша ссылка для восстановления пароля: ' . Html::a($url, $url))
            ->send();
    }

    /**
     * @param string $token
     * @param array $data
     * @return array|User|null|\yii\db\ActiveRecord
     * @throws \Exception
     */
    public function resetPasswordByToken(string $token, array $data = [])
    {
        $modelForm = new ResetPasswordForm();
        $modelForm->load($data, '');
        $modelForm->token = $token;

        if (!$modelForm->validate()) {
            throw new Exception(implode(',', $modelForm->firstErrors));
        }

        /** Защищаем от подбора токена, путём полного перебора*/
        $user = User::find()->where(['password_reset_token' => $modelForm->token])->one();
        if (!$user) {
            throw new NotFoundHttpException();
        }

        $user->removePasswordResetToken();
        $user->setPassword($modelForm->password);
        if(!$user->save()) {
            throw new \Exception(implode(',', $this->user->firstErrors));
        }

        return $user;
    }
}

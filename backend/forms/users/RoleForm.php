<?php
namespace backend\forms\users;

use common\models\users\User;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Форма
 * Class RoleForm
 * @package quartz\user\models\forms
 */
class RoleForm extends Model
{
    public $roles = [];             //список ролей для
    public $permissions = [];
    public $id_user;

    /**
     * @return array массив валидации
     */
    public function rules()
    {
        $rules = [
            //todo можно добавить проверку на роли/прав
            'rolesAndPermisions' => [['roles','permissions'], 'safe'],
            'integer' => [['id_user'], 'integer'],
            'required' => [['id_user'], 'required'],
            'exist_user' => ['id_user', 'exist', 'targetClass' => User::class, 'targetAttribute' => 'id'],
        ];
        return $rules;
    }

    /**
     * Массив лэйблов локализованный
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'roles' => 'Роли',
            'permissions' => 'Права'
        ];
    }

    /**
     * Подготовка перед обновлением
     * @param User $user
     */
    public function prepareUpdate(User $user)
    {
        $this->id_user = $user->id;
        $this->roles = array_keys(Yii::$app->authManager->getRolesByUser($user->id));
        $this->permissions = array_keys(Yii::$app->authManager->getAssignments($user->id));
    }

    /**
     * @return array
     */
    public function getRolesDataList()
    {
        return ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description');
    }

    /**
     * @return array
     */
    public function getPermissionsDataList()
    {
        return ArrayHelper::map(Yii::$app->authManager->getPermissions(), 'name', 'description');
    }
}

<?php

namespace backend\forms\users;

use common\models\users\User;
use yii\base\Model;

/**
 * Class UserForm
 * @package backend\forms\users
 */
class UserForm extends Model
{
    public $id;
    public $email;
    public $password;
    public $username;
    public $status;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['password'], 'string', 'max' => 255],
            [['email', 'username', 'status'], 'required'],
            [['email', 'password', 'username'], 'string', 'max' => 255],
            [['status'], 'integer'],
            [['email'], 'validateEmail'],
            [['email'], 'email'],
        ];
    }

    public function validateEmail() : void
    {
        $user = User::findByEmail($this->email, $this->id);
        if($user) {
            $this->addError('email', 'Email уже занят');
        }
    }

    public function prepareCreate(User $user)
    {
        $this->id = $user->id;
        $this->setAttributes($user->attributes);
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'status' => 'Статус',
            'username' => 'Логин',
        ];
    }

}
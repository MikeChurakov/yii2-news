<?php

namespace backend\forms\users;

use yii\base\Model;

/**
 * Class PasswordResetRequestForm
 * @package backend\forms\users
 */
class PasswordResetRequestForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'filter_1' => ['email', 'filter', 'filter' => 'trim'],
            'filter_2' => ['email', 'filter', 'filter' => 'strip_tags'],
            'filter_3' => ['email', 'filter', 'filter' => 'strtolower'],
            'require' => ['email', 'required', 'message' => 'Необходимо заполнить email'],
            'email' => ['email', 'email'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Email',
        ];
    }
}

<?php

namespace backend\forms\users;

use yii\base\Model;

/**
 * Class ResetPasswordForm
 * @package backend\forms\users
 */
class ResetPasswordForm extends Model
{
    public $password;
    public $passwordConfirm;
    public $token;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            'filter_1' => [['password', 'passwordConfirm', 'token'], 'filter', 'filter' => 'trim'],
            'filter_2' => [['password', 'passwordConfirm', 'token'], 'filter', 'filter' => 'strip_tags'],
            'require' => [['password', 'passwordConfirm', 'token'], 'required'],
            'string' => [['token'], 'string'],
            'compare' => [
                ['passwordConfirm'],
                'compare',
                'compareAttribute' => 'password',
                'message' => 'Пароли должны совпадать'
            ],
            'maxLength' => [
                ['password', 'passwordConfirm'],
                'string', 'min' => 6, 'max' => 72,
                'tooShort' => 'Пароль не может быть короче 6 символов',
                'tooLong' => 'Пароль не может быть длиннее 72 символов'
            ]
        ];

        return $rules;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'passwordConfirm' => 'Повторите пароль',
        ];
    }

    /**
     * При ошибочной валидации чистим атрибуты паролей
     */
    public function afterValidate()
    {
        if ($this->hasErrors()) {
            $this->password = null;
            $this->passwordConfirm = null;
        }

        parent::afterValidate();
    }
}

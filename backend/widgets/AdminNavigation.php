<?php
namespace backend\widgets;

use yii\base\Widget;

/**
 * Виджет меню админки
 *
 * Class AdminNavigation
 * @package backend\components\widgets
 */
class AdminNavigation extends Widget
{
    /**
     * Массив для пунктов меню
     * @var array
     */
    public $items = [
        [
            'label' => 'Главная',
            'url' => '/admin',
            'class' => 'fa fa-home',
            'routes' => ['site/index']
        ],
        [
            'label' => 'Пользователи',
            'url' => 'javascript:;',
            'class' => 'fa fa-users',
            'routes' => ['users/index', 'users/create'],
            'subItems' => [
                [
                    'label' => 'Список',
                    'url' => '/admin/users/index',
                    'routes' => ['users/index'],
                ],
                [
                    'label' => 'Добавить',
                    'url' => '/admin/users/create',
                    'routes' => ['users/create'],
                ],
            ],
        ],
        [
            'label' => 'Новости',
            'url' => 'javascript:;',
            'class' => 'fa fa-address-card',
            'routes' => ['news/index', 'news/create', 'news/update', 'news/view'],
            'subItems' => [
                [
                    'label' => 'Список',
                    'url' => '/admin/news/index',
                    'routes' => ['news/index'],
                ],
                [
                    'label' => 'Добавить',
                    'url' => '/admin/news/create',
                    'routes' => ['news/create'],
                ],
            ]
        ],
    ];

    /**
     * Инициализация виджета
     *
     * @param array $items
     */
    public function init(array $items = [])
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('admin-navigation/main', [
            'items' => $this->items,
        ]);
    }
}

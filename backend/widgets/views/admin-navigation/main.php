<ul class="nav metismenu" id="side-menu">
    <li class="nav-header">
        <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="/admin/img/profile_small.jpg">
                             </span>
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David Williams</strong>
                             </span> <span class="text-muted text-xs block">Art Director</span> </span> </a>
        </div>
        <div class="logo-element">
            IN+
        </div>
    </li>
<!--    <li>-->
<!--        <a href="index.html"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span> <span class="fa arrow"></span></a>-->
<!--        <ul class="nav nav-second-level collapse">-->
<!--            <li><a href="index.html">Dashboard v.1</a></li>-->
<!--            <li><a href="dashboard_2.html">Dashboard v.2</a></li>-->
<!--            <li><a href="dashboard_3.html">Dashboard v.3</a></li>-->
<!--            <li><a href="dashboard_4_1.html">Dashboard v.4</a></li>-->
<!--            <li><a href="dashboard_5.html">Dashboard v.5 </a></li>-->
<!--        </ul>-->
<!--    </li>-->
<!--    <li>-->
<!--        <a href="layouts.html"><i class="fa fa-diamond"></i> <span class="nav-label">Layouts</span></a>-->
<!--    </li>-->
<!--    <li class="active">-->
<!--        <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Graphs</span><span class="fa arrow"></span></a>-->
<!--        <ul class="nav nav-second-level collapse in">-->
<!--            <li class="active"><a href="graph_flot.html">Flot Charts</a></li>-->
<!--            <li><a href="graph_morris.html">Morris.js Charts</a></li>-->
<!--        </ul>-->
<!--    </li>-->
<!--    --><?//= Yii::$app->requestedRoute ?>
<!--    --><?php //die(); ?>
    <?php
    $route = Yii::$app->controller->id . '/' . Yii::$app->controller->action->id;
//    die($route);
    ?>
    <?php foreach ($items as $item) : ?>
        <li class="<?= in_array($route, $item['routes']) ? 'active' : '' ?>">
            <a href="<?= $item['url'] ?? '' ?>"><i class="<?= $item['class'] ?? '' ?>">
                </i> <span class="nav-label"><?= $item['label'] ?? '' ?></span>
                <?php if(!empty($item['subItems'])) : ?>
                    <span class="fa arrow"></span>
                <?php endif; ?>
            </a>
            <?php if(!empty($item['subItems'])) : ?>
                <ul class="nav nav-second-level collapse">
                    <?php foreach ($item['subItems'] as $subItem) : ?>
                        <li class="<?= in_array($route, $subItem['routes']) ? 'active' : '' ?>"><a href="<?= $subItem['url'] ?? '' ?>"><?= $subItem['label'] ?? '' ?></a></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

        </li>
    <?php endforeach; ?>

<!--    <li>-->
<!--        <a href="css_animation.html"><i class="fa fa-magic"></i> <span class="nav-label">CSS Animations </span><span class="label label-info pull-right">62</span></a>-->
<!--    </li>-->
<!--    <li class="landing_link">-->
<!--        <a target="_blank" href="landing.html"><i class="fa fa-star"></i> <span class="nav-label">Landing Page</span> <span class="label label-warning pull-right">NEW</span></a>-->
<!--    </li>-->
<!--    <li class="special_link">-->
<!--        <a href="package.html"><i class="fa fa-database"></i> <span class="nav-label">Package</span></a>-->
<!--    </li>-->
</ul>
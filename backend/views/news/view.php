<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\news\News;

/* @var $this yii\web\View */
/* @var $model common\models\news\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить новость',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'slug',
            'description',
            'content:html',
            [
                'label' => 'Автор',
                'attribute' => 'user_id',
                'value' => function (News $row) {
                    return $row->user->username;
                }
            ],
            'user_id',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>

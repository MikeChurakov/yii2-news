<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\fileapi\Widget as FileAPI;
use common\models\users\User;

/* @var $this yii\web\View */
/* @var $model common\models\news\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->widget(\mihaildev\ckeditor\CKEditor::class, [
        'editorOptions' => \mihaildev\elfinder\ElFinder::ckeditorOptions('elfinder', [
            'removeButtons' => 'Underline,JustifyCenter,Font'
        ]),
    ]); ?>

    <?= $form->field($model, 'image')->widget(
        FileAPI::class,
        [
            'settings' => [
                'url' => ['/upload/fileapi-upload']
            ]
        ]
    ); ?>

    <?= $form->field($model, 'user_id')->dropDownList(User::getList()) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

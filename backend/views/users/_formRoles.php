<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use nex\chosen\Chosen;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $roleForm \backend\forms\users\RoleForm */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'id' => 'form-role-update',
    'action' => Url::to(['/users/role-save', 'id' => $roleForm->id_user]),
    'options' => ['enctype' => 'multipart/form-data'],
]); ?>
<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($roleForm, 'roles')->widget(Chosen::className(), [
                    'placeholder' => 'Роли',
                    'items' => $roleForm->getRolesDataList(),
                    'multiple' => true
                ]); ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($roleForm, 'permissions')->widget(Chosen::className(), [
                    'placeholder' => 'Права',
                    'items' => $roleForm->getPermissionsDataList(),
                    'multiple' => true
                ]); ?>
            </div>
        </div>
    </div>

    <div class="box-footer text-left">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success', 'name' => 'save-button']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\users\User */

$this->title = 'Обновление пользователя: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновление';

$items[] = [
    'label' => 'Профиль',
    'content' => $this->render('_form', ['model' => $model]),
];

    $items[] = [
        'label' => 'Права',
        'content' => $this->render('_formRoles', ['roleForm' => $roleForm]),
    ];
?>
<?php echo \yii\bootstrap\Tabs::widget([
    'items' => $items,
    'options' => ['tag' => 'div', 'class' => ''],
    'itemOptions' => ['tag' => 'div'],
    'headerOptions' => ['class' => ''],
    'clientOptions' => ['collapsible' => false],
]);

?>

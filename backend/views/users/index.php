<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\users\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <p>
        <?= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= \kartik\dynagrid\DynaGrid::widget([
        'gridOptions' => [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'username',
                'label' => 'Логин',
            ],
            'email:email',
            [
                'attribute' => 'status',
                'label' => 'Статус',
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Создано',
            ],
            [
                'attribute' => 'updated_at',
                'label' => 'Обновлено',
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
        'options'=>['id'=>'dynagrid-users'],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Восстановление пароля';

?>

<div class="middle-box text-center loginscreen">
    <div>
        <h3><?=$this->title?></h3>
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => [
                'class' => 'm-t'
            ]
        ]); ?>
        <div class="row">
            <?= \common\widgets\Alert::widget() ?>
        </div>
        <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>
        <?= $form->field($model, 'passwordConfirm')->passwordInput() ?>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary block full-width m-b', 'name' => 'login-button']) ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>

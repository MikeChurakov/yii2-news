<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\users\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
<div class="middle-box text-center loginscreen">
    <div>
        <h3><?=$this->title?></h3>
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => [
                'class' => 'm-t'
            ]
        ]); ?>
            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'rememberMe')->checkbox() ?>
        <?= Html::submitButton('Войти', ['class' => 'btn btn-primary block full-width m-b', 'name' => 'login-button']) ?>

        <a href="<?= \yii\helpers\Url::to(['/site/request-password-reset'])?>"><small>Забыли пароль?</small></a>
        <?php ActiveForm::end(); ?>
    </div>
</div>

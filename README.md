Требования
-------------
#### php >= 7.2
#### php-intl - для корректной работы SlugBehaviour

Установка
----------
1. git clone
1. composer install
1. php init
1. создать бд, и прописать свои конфиги в common/config/main-local.php
1. php yii migrate --migrationPath=@yii/rbac/migrations
1. php yii migrate
1. логин/пароль админ панели - admin/123456

#### Примеры других проектов можно посмотреть на  [github - аккаунте](https://github.com/Churakovmike)
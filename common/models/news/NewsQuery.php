<?php

namespace common\models\news;

/**
 * This is the ActiveQuery class for [[News]].
 *
 * @see News
 */
class NewsQuery extends \yii\db\ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return News[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return News|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param string $slug
     * @return NewsQuery
     */
    public function bySlug(string $slug): self
    {
        return $this->andWhere(['slug' => $slug]);
    }
}

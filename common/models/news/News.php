<?php

namespace common\models\news;

use common\behaviors\ImageBehavior;
use common\models\users\User;
use vova07\fileapi\behaviors\UploadBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "news".
 *
 * @property int $id Первичный ключ
 * @property string $title Название новости
 * @property string $slug Слаг/ЧПУ для новости
 * @property string $description Краткое описание
 * @property string $content Контент новости
 * @property string $image Путь к превью новости
 * @property int $user_id Автор новости
 * @property int $created_at Дата создания новости
 * @property int $updated_at Дата обновления новости
 *
 * @property User $user
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'content', 'user_id'], 'required'],
            [['content'], 'string'],
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['title', 'slug', 'description', 'image'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'slug' => 'ЧПУ',
            'description' => 'Описание',
            'content' => 'Контент',
            'image' => 'Превью',
            'user_id' => 'Автор',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'uploadBehavior' => [
                'class' => UploadBehavior::class,
                'attributes' => [
                    'image' => [
                        'path' => '@frontend/web/upload/news/',
                        'tempPath' => '@frontend/web/upload/temp/',
                        'url' => '/upload/news/',
                    ],
                ]
            ],
            [
                'class' => ImageBehavior::class,
                'imageColumn' => 'image',
                'imagePath' => '/upload/news/',
            ],
            [
                'class' => SluggableBehavior::class,
                'attribute' => 'title',
            ],
        ];
    }

    /**
     * Belong To User Relation
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return NewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NewsQuery(get_called_class());
    }

    /**
     * @return false|string
     */
    public function getCreatedAt()
    {
        return date('d-m-Y H:i', $this->created_at);
    }

    /**
     * @return false|string
     */
    public function getUpdatedAt()
    {
        return date('d-m-Y H:i', $this->updated_at);
    }

    /**
     * @param string $size
     * @return string
     */
    public function getImageUrl($size = '')
    {
        if (empty($this->image) || is_null($this->image)) {
            $this->imagePath = '/img/stub/';
            $this->image = 'news.jpg';
        }

        return $this->getUri($size, true, true);
    }
}

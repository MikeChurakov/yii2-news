<?php

namespace common\behaviors;

use Yii;
use yii\helpers\FileHelper;

/**
 * Class Image
 * @package common\behaviors
 */
class Image
{
    /**
     * @var string Путь для хранения кэшированных изображений
     */
    private static $uploadDir = '/upload/cache/';

    /**
     * Возвращает url для закэшированных картинок
     * если не найдено, то кэширует картинку
     * @param $filePath
     * @param null $size
     * @param bool $ratio
     * @param bool $crop
     * @param int $rgb
     * @return null|string
     */
    static public function getUri($filePath, $size = null, $ratio = true, $crop = false, $rgb = 0xFFFFFF)
    {
        $url = null;

        if ($size) {
            $fileName = pathinfo($filePath, PATHINFO_FILENAME);  // Название файла
            $fileExt = '.' . pathinfo($filePath, PATHINFO_EXTENSION);  // Расширение файла
            $fileUniqueName = crc32($filePath); //Уникальное имя файла
            if ($fileName && $fileExt) {
                $url = self::$uploadDir . $fileUniqueName . '_' . $size . $fileExt; // Путь к необходимой картинке
            }

            if (!is_file(Yii::getAlias('@frontend/web/upload') . $url)) {
                // Кэшируем картинку, если не найдено и если существует оригинал картинки
                if (is_file(Yii::getAlias('@frontend/web/upload') . $filePath)) {
                    var_dump(Yii::getAlias('@frontend/web/upload') . $filePath);die;

                    Image::imageCache($filePath, $size, $url, $ratio, $crop, $rgb);
                } else {
                    $url = null;
                }
            }
        }
        return $url;
    }

    /**
     * Кэширование изображения
     * @param $filePath
     * @param $size
     * @param $url
     * @param bool $ratio
     * @param bool $crop
     * @param int $rgb
     * @return bool
     */
    public static function imageCache($filePath, $size, $url, $ratio = true, $crop = false, $rgb = 0xFFFFFF)
    {
        $sizes = explode('x', $size);
        $width = $sizes[0] ? $sizes[0] : null;
        $height = $sizes[1] ? $sizes[1] : null;

        $fileIn = Yii::getAlias('@frontend/web/') . $filePath; // Путь для загрузки изображения
        $fileOut = Yii::getAlias('@frontend/web/') . $url;   // Путь для сохранения изображения

        if ($width && $height) {
            $image = self::imageCropAndScale($fileIn, $width, $height, $ratio, $crop, $rgb);
        } else {
            $image = self::imageResize($fileIn, $width, $height);
        }

        self::imageSave($image, $fileOut, exif_imagetype($fileIn));

        if (is_null($image)) {
            return false;
        }
        return true;
    }

    /**
     * Обрезание и масштабирование изображения
     * @param $filePath - Путь к изображению
     * @param $width - Новая ширина
     * @param $height - Новая высота
     * @param bool $ratio - Сохранять соотношение сторон ?
     * @param bool $crop - Обрезать картинку ?
     * @param int $rgb - Цвет заливки фона картинки при изменнии размера
     * @return bool|resource
     */
    private static function imageCropAndScale($filePath, $width, $height, $ratio = true, $crop = false, $rgb = 0xFFFFFF)
    {
        if (!is_file($filePath)) {
            return false;
        }

        $image = self::imageLoad($filePath, exif_imagetype($filePath)); // Создаем новое изображение из файла

        list($w, $h) = getimagesize($filePath); // Берем высоту и ширину изображения

        if (!$w or !$h) {
            return false;
        }

        // Вычисляем координаты для масштабирования
        $iniP = $w / $h;
        if (!$width && $height) {
            $width = ($height * $w) / $h;
        } else if (!$height && $width) {
            $height = ($width * $h) / $w;
        } else if (!$height && !$width) {
            $width = $w;
            $height = $h;
        }

        $dstX = $dstY = 0;
        $srcX = $srcY = 0;
        $resP = $width / $height;
        // Обрезаем картинку
        if ($crop) {
            $dstW = $width;
            $dstH = $height;
            if ($iniP > $resP) {
                $srcH = $h;
                $srcW = $h * $resP;
                $srcX = ($w >= $srcW) ? floor(($w - $srcW) / 2) : $srcW;
            } else {
                $srcW = $w;
                $srcH = $w / $resP;
                $srcY = ($h >= $srcH) ? floor(($h - $srcH) / 2) : $srcH;
            }
        } else {
            if ($iniP > $resP) {
                $dstW = $width;
                $dstH = $ratio ? floor($dstW / $w * $h) : $height;
                $dstY = $ratio ? floor(($height - $dstH) / 2) : 0;
            } else {
                $dstH = $height;
                $dstW = $ratio ? floor($dstH / $h * $w) : $width;
                $dstX = $ratio ? floor(($width - $dstW) / 2) : 0;
            }
            $srcW = $w;
            $srcH = $h;
        }

        $imageOutput = imagecreatetruecolor($width, $height);   // Создаем конечную картинку
        if ((exif_imagetype($filePath) == IMAGETYPE_PNG || exif_imagetype($filePath) == IMAGETYPE_GIF)) {
            imagealphablending($imageOutput, false);
            imagesavealpha($imageOutput, true);
            imagefill($imageOutput, 0, 0, IMG_COLOR_TRANSPARENT);
            imagealphablending($image, true);
        } else {
            imagefill($imageOutput, 0, 0, $rgb);
        }
        imagecopyresampled($imageOutput, $image, $dstX, $dstY, $srcX, $srcY, $dstW, $dstH, $srcW, $srcH);   // Копирование и изменение размера изображения с ресемплированием
        imagedestroy($image);   //Освобождаем память

        return $imageOutput;
    }

    /**
     * Загрузка изображения
     * @param $filePath
     * @param $imageType
     * @return resource
     */
    private static function imageLoad($filePath, $imageType)
    {
        switch ($imageType) {
            case IMAGETYPE_GIF:
                return imagecreatefromgif($filePath);
            case IMAGETYPE_JPEG:
                $image = @imagecreatefromjpeg($filePath);
                if (!$image) {
                    self::checkJpeg($filePath, true);
                    return imagecreatefromjpeg($filePath);
                }
                return $image;
            case IMAGETYPE_PNG:
                return imagecreatefrompng($filePath);
            default:
                return null;
        }
    }

    /**
     * try fix error "Premature end of file" with Jpeg images
     * @param $f
     * @param bool $fix
     * @return bool
     */
    function checkJpeg($f, $fix = false)
    {
        # check for jpeg file header and footer - also try to fix it
        if (false !== (@$fd = fopen($f, 'r+b'))) {
            if (fread($fd, 2) == chr(255) . chr(216)) {
                fseek($fd, -2, SEEK_END);
                if (fread($fd, 2) == chr(255) . chr(217)) {
                    fclose($fd);
                    return true;
                } else {
                    if ($fix && fwrite($fd, chr(255) . chr(217))) {
                        return true;
                    }
                    fclose($fd);
                    return false;
                }
            } else {
                fclose($fd);
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Сохранение изображения
     * @param $image
     * @param $filePath
     * @param $imageType
     * @param int $quality
     * @return bool
     */
    private static function imageSave($image, $filePath, $imageType, $quality = 100)
    {
        $dir = pathinfo($filePath, PATHINFO_DIRNAME);
        if (!file_exists($dir)) {
            FileHelper::createDirectory($dir);
        }

        switch ($imageType) {
            case IMAGETYPE_GIF:
                $result = imagegif($image, $filePath, $quality);
                imagedestroy($image); //Освобождаем память
                break;
            case IMAGETYPE_JPEG:
                $result = imagejpeg($image, $filePath, $quality);
                imagedestroy($image); //Освобождаем память
                break;
            case IMAGETYPE_PNG:
                $result = imagepng($image, $filePath, ($quality >= 10) ? ($quality / 10) - 1 : 0);
                imagedestroy($image); //Освобождаем память
                break;
            default:
                $result = false;
                break;
        }
        return $result;
    }

    /**
     * Пропорциональное изменение изображение по высоте или ширине
     * @param $filePath
     * @param $width
     * @param $height
     * @return resource
     */
    private static function imageResize($filePath, $width, $height)
    {
        // Создаем новое изображение из файла
        $image = self::imageLoad($filePath, exif_imagetype($filePath));

        list($w, $h) = getimagesize($filePath); // Берем высоту и ширину изображения
        // Вычисляем коэффициент ширина или высота которая должна быть
        if ($width) {
            $ratio = $w / $width;
            $height = ceil($h / $ratio); // с помощью коэффициента вычисляем высоту
        } else {
            $ratio = $h / $height;
            $width = ceil($w / $ratio); // с помощью коэффициента вычисляем ширину
        }

        $imageOutput = imagecreatetruecolor($width, $height);    // Создаем картинку
        imagecopyresampled($imageOutput, $image, 0, 0, 0, 0, $width, $height, $w, $h); // Копирование и изменение размера изображения с ресемплированием
        imagedestroy($image);   //Освобождаем память

        return $imageOutput;
    }

    /**
     * Пропорционально изменяет размер картинки по заданному размеру, если картинка больше указанного размера
     * @param $file
     * @param $size
     */
    public static function imageAutoResize($file, $size)
    {
        list($w, $h) = getimagesize($file);
        if ($w > $size || $h > $size) {
            if ($w > $h) {
                $image = self::imageResize($file, $size, null);
            } else {
                $image = self::imageResize($file, null, $size);
            }
            self::imageSave($image, $file, exif_imagetype($file));
        } else {
            file_put_contents($file, file_get_contents($file));
        }
    }


    /**
     * Загружает картинку из url в файл
     * @param $link
     * @param $filePath string полный путь куда сохранить картинку /var/www/project/upload/temp/51323124.jpg
     * @return null|string
     */
    public static function loadFromUrlAndSave($link, $filePath)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        /** Если не удалось ничего загрузить возвращаем ошибку */
        if($result){

            if($savefile = fopen($filePath, 'w')){
                fwrite($savefile, $result);
                fclose($savefile);
                return $filePath;
            }
        }
        /**  если не удалось загрузить или сохранить возвращаем ошибку */
        return null;
    }
}

<?php

namespace common\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * Class ImageBehavior
 * @package common\behaviors
 *
 * @property string $imageColumn
 * @property string $imagePath
 * @property string $uploadDir
 */
class ImageBehavior extends Behavior
{
    /**
     * @var string Название поля в таблице с картинкой
     */
    public $imageColumn;

    /**
     * @var string Путь к директории с картинками
     */
    public $imagePath;

    /**
     * @var string Путь для хранения кэшированных изображений
     */
    public $uploadDir = '/upload/temp/';


    private function getImagePath(){
        return $this->imagePath;
    }

    /**
     * Возвращает url для закэшированных картинок
     * если не найдено, то кэширует картинку
     * @param bool $size
     * @param bool $ratio
     * @param bool $crop
     * @param int $rgb
     * @return null|string
     */
    public function getUri($size = null, $ratio = true, $crop = false, $rgb = 0xFFFFFF)
    {
        $url = null;
        if($this->imageColumn && $this->imagePath) {

            $imagePath = $this->getImagePath();

            /** @var ActiveRecord $model */
            $model = $this->owner;

            if($size) {
                $fileName = pathinfo($model->getAttribute($this->imageColumn), PATHINFO_FILENAME);  // Название файла
                $fileExt = '.' . pathinfo($model->getAttribute($this->imageColumn), PATHINFO_EXTENSION);  // Расширение файла
                if($fileName && $fileExt) {
                    $url = $this->uploadDir . $fileName . '_' . $size . $fileExt; // Путь к необходимой картинке
                }

                if (!is_file(\Yii::getAlias('@frontend/web/upload') . $url)) {
                    // Кэшируем картинку, если не найдено и если существует оригинал картинки
                    if(is_file(\Yii::getAlias('@frontend/web/') . $imagePath . $model->getAttribute($this->imageColumn))) {
                        if  ((@Image::imageCache($imagePath . $model->getAttribute($this->imageColumn), $size, $url, $ratio, $crop, $rgb)) == false) {
                            $url = null;
                        };
                    } else {
                        $url = null;
                    }
                }
            } else {
                $url = $imagePath . $model->getAttribute($this->imageColumn);
            }
        }
        return $url;
    }

    /**
     * Сохраняет путь к директории с картинками
     * @param string $path
     */
    public function setImagePath($path)
    {
        $this->imagePath = $path;
    }

    /**
     * Сохраняет название поля в таблице с картинкой
     * @param string $name
     */
    public function setImageColumn($name)
    {
        $this->imageColumn = $name;
    }
}
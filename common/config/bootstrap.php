<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

$container = Yii::$container;
$container->setDefinitions([
    \frontend\services\news\NewsService::class => \frontend\services\news\NewsService::class,
    \backend\services\news\NewsService::class => \backend\services\news\NewsService::class,
]);

<?php

namespace frontend\controllers;

use frontend\services\news\NewsService;
use yii\base\Module;

/**
 * Class NewsController
 * @package frontend\controllers
 *
 * @property NewsService $newsService
 */
class NewsController extends Controller
{
    /**
     * @var NewsService $newsService
     */
    protected $newsService;

    /**
     * NewsController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     * @param NewsService $service
     */
    public function __construct(string $id, Module $module, array $config = [], NewsService $service)
    {
        $this->newsService = $service;
        parent::__construct($id, $module, $config);
    }

    /**
     * Страница списка новостей
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', $this->newsService->getLast());
    }

    /**
     * Страница просмотра новостей
     *
     * @param string $slug
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView(string $slug)
    {
        return $this->render('view', [
            'newsItem' => $this->newsService->getBySlug($slug),
        ]);
    }
}

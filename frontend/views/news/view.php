<?php
/** @var \common\models\news\News $newsItem */
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1><?= $newsItem->title ?></h1>
        </div>
        <div class="col-md-12">
            <span>Добавлено: <?= $newsItem->getCreatedAt() ?></span>
        </div>
        <div class="col-md-12">
            <p><?= $newsItem->content ?></p>
        </div>
    </div>
</div>

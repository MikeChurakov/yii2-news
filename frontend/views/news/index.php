<?php

use yii\helpers\Url;

/** @var \common\models\news\News[] $news */
?>

<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-9">
            <p class="pull-right visible-xs">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
            </p>
            <div class="jumbotron">
                <h1>Последние новости</h1>
            </div>
            <div class="row">
                <?php foreach ($news as $newsItem) : ?>
                    <div class="col-xs-6 col-lg-4">
                        <div class="img">
                            <img src="<?= $newsItem->getImageUrl('340x250') ?>" alt="">
                        </div>
                        <h2><?= $newsItem->title ?></h2>
                        <p><?= $newsItem->description ?></p>
                        <p><a class="btn btn-default" href="<?= Url::to(['news/view', 'slug' => $newsItem->slug]) ?>" role="button">Подробнее &raquo;</a></p>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <?= \yii\widgets\LinkPager::widget([
                'pagination' => $pages,
            ]) ?>
        </div>
    </div>
</div>

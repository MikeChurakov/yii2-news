<?php

namespace frontend\services\news;

use common\models\news\News;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

/**
 * Class NewsService
 * @package frontend\services\news
 */
class NewsService
{
    protected CONST DEFAULT_LIMIT = 6;

    /**
     * @return array
     */
    public function getLast(): array
    {
        $news = News::find()
            ->orderBy(['id' => SORT_DESC]);
        $newsCount = clone $news;
        $pages = new Pagination(['totalCount' => $newsCount->count(), 'pageSize' => self::DEFAULT_LIMIT]);
        $news->offset($pages->offset)
            ->limit($pages->limit);

        return [
            'news' => $news->all(),
            'pages' => $pages,
        ];
    }

    /**
     * @param string $slug
     * @return News|null
     * @throws NotFoundHttpException
     */
    public function getBySlug(string $slug): ?News
    {
        $newsItem = News::find()
            ->bySlug($slug)
            ->one();

        if (is_null($newsItem)) {
            throw new NotFoundHttpException('Запрашиваемая новость удалена или перемещена в другой раздел');
        }

        return $newsItem;
    }
}

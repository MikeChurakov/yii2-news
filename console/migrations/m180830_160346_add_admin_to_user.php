<?php

use yii\db\Migration;

/**
 * Class m180830_160346_add_admin_to_user
 */
class m180830_160346_add_admin_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $query = (new \yii\db\Query())
            ->select('id')
            ->from('user')
            ->where(['username' => 'admin']);

        $adminId =  $query->scalar();

        $role = Yii::$app->authManager->createRole('admin');
        $role->description = 'Админ';
        Yii::$app->authManager->add($role);

        $userRole = Yii::$app->authManager->getRole('admin');
        Yii::$app->authManager->assign($userRole, $adminId);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}

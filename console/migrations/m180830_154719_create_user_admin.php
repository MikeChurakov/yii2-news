<?php

use yii\db\Migration;

/**
 * Class m180830_154719_create_user_admin
 */
class m180830_154719_create_user_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->delete('user');
        $this->batchInsert('user', [
            'username',
            'auth_key',
            'password_hash',
            'email',
            'status'
        ], [
            [
                'admin',
                Yii::$app->security->generateRandomString(),
                Yii::$app->security->generatePasswordHash('123456'),
                'admin@site.com',
                10,
            ]
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('user', ['username' => 'admin']);
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m200526_045517_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey()->comment('Первичный ключ'),
            'title' => $this->string(255)->notNull()->comment('Название новости'),
            'slug' => $this->string(255)->comment('Слаг/ЧПУ для новости'),
            'description' => $this->string(255)->notNull()->comment('Краткое описание'),
            'content' => $this->text()->notNull()->comment('Контент новости'),
            'image' => $this->string(255)->comment('Путь к превью новости'),
            'user_id' => $this->integer()->notNull()->comment('Автор новости'),
            'created_at' => $this->integer()->defaultValue(time())->comment('Дата создания новости'),
            'updated_at' => $this->integer()->comment('Дата обновления новости'),
        ]);

        $this->addForeignKey(
            'news_user_fk',
            'news',
            'user_id',
            'user',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('news_user_fk', 'news');
        $this->dropTable('news');
    }
}
